<!doctype html>
<html>
<head>
	<title>Road to regalo!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="main.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
	<h1>Dashboar</h1>
	<br>
	<p>Complimenti per aver risolta la Challenge 0</p>
	<p>Risolvi tutte le challenge per ricevere il premio finale</p>

	<div class="container">
		<h2>Challenge 1</h2>
		<a href="challenges/challenge1.php">Follow this link</a>
		<form action="flags/flag1.php" method="post">
			<label>Flag:</label>
			<input type="text" placeholder="MDG21{...}" class="form-control" name="flag">
			<input type="submit" value="Submit" class="btn btn-primary">
		</form>
	</div>
	</br>
	<div class="container">
		<h2>Challenge 2</h2>
		<a href="challenges/challenge2.php">Follow this link</a>
		<form action="flags/flag2.php" method="post">
			<label>Flag:</label>
			<input type="text" placeholder="MDG21{...}" class="form-control" name="flag">
			<input type="submit" value="Submit" class="btn btn-primary">
		</form>
	</div>
	</div>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
